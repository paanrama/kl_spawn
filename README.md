# KL Spawn
Minetest player spawn module for Gaël de Sailly mapgen_rivers map generator.

## Author
Code: Pablo Raffo

Inspired on previous work of:
- Nathan Salapat random_spawn mod
- Minenux minetest_mod_spawnrand mod

Special thanks to Gaël de Silly who develop mapgen_rivers and biomegen minetest modules.

## License
License: GNU LGPLv3.0

## Requirements
mapgen_rivers by Gaël de Sailly

## Installation
This mod should be placed in the mods/ directory of Minetest like any other mod.

## Settings
No settings yet, but it use some local variables that will be settings soon.

