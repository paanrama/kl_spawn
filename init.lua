-- KL Spawn
-- Inspired on random_spawn mod by Nathan Salapat and minetest_mod_spawnrand by minenux
-- TODO: Add checks for protection, adjacent nodes required, etc.
-- TODO: Add (to respawn) compatibility with more set home logic (beds or other)

kl_spawn = {}

-- One of the following: "none", "error", "warning", "action", "info", or "verbose"
local debug_log_level = "info"
kl_spawn.dev_mode = false

-- compatibility with sethome modules
kl_spawn.sethome_installed = minetest.get_modpath("sethome") or minetest.get_modpath("kl_sethome")

local mapgen_limit = tonumber(minetest.settings:get("mapgen_limit")) or 30000
kl_spawn.radius_max = 15000
if kl_spawn.radius_max > mapgen_limit then
    kl_spawn.radius_max = mapgen_limit
end

kl_spawn.radius_inc = 1000
local max_cycles = math.ceil(kl_spawn.radius_max / kl_spawn.radius_inc)
kl_spawn.spawn_count_per_block = 3     -- block = kl_spawn.radius_inc * kl_spawn.radius_inc area
kl_spawn.min_spawn_elevation = 1
kl_spawn.max_spawn_elevation = 100
kl_spawn.emerge_radius = 30
kl_spawn.emerge_area_delay = 0.5
kl_spawn.new_player_wait_time = 5
kl_spawn.new_player_max_intents = 10

-- valid spawn nodes: a list of node names or groups
-- the nodes must have a group value of 1 to be included
kl_spawn.valid_spawn_node_list = {"group:soil", "group:sand"}


-- mapgen rivers data
local blocksize = mapgen_rivers.settings.blocksize
local X = math.floor(mapgen_rivers.settings.map_x_size / blocksize)
local Z = math.floor(mapgen_rivers.settings.map_z_size / blocksize)
local sea_level = mapgen_rivers.settings.sea_level


-- convert (x, y) to mapgen_rivers dem grid index
local function to_grid_dem_index(x, z)
	return z*X+x+1
end


-- check mapgen_rivers grid to see if terrain height is a valid spawn pos
local function has_valid_height(x, z)
    local grid = mapgen_rivers.grid
    local x_grid = math.max(math.floor((x+blocksize*grid.size.x/2)/blocksize), 0)
    local z_grid = math.max(math.floor((z+blocksize*grid.size.y/2)/blocksize), 0)
    if x_grid > (X - 2) then
        x_grid = X - 2
    end
    if z_grid > (Z - 2) then
        z_grid = Z - 2
    end

    local terrain_height = grid.dem[to_grid_dem_index(x_grid, z_grid)]
    minetest.log(debug_log_level, string.format("[kl_spawn] Position (%d, %d) has terrain height of %d", x, z, terrain_height))
    if terrain_height > kl_spawn.min_spawn_elevation and terrain_height < kl_spawn.max_spawn_elevation then
        return true
    else
        return false
    end
end


-- get a random (x, z) position
-- each cycle representa a square shape band of wide kl_spawn.radius_inc that go away of (0, 0)
-- cycle 1 is a circle with center on (0, 0)
-- cycle n is a donut with center on (0, 0) that not include the previous cycle
function kl_spawn:get_random_pos(cycle)
    local posx, posz, ax, az, bx, bz
    ax = math.random(-kl_spawn.radius_inc, kl_spawn.radius_inc)
    az = math.random(-kl_spawn.radius_inc, kl_spawn.radius_inc)
    if ax == 0 then
        ax = 1
    end
    if az == 0 then
        az = 1
    end
    local axMag = math.abs(ax)
    local azMag = math.abs(az)
    if axMag > azMag then
        bx = kl_spawn.radius_inc * (cycle -1)
        bz = math.floor(azMag * bx / axMag)
    else
        bz = kl_spawn.radius_inc * (cycle - 1)
        bx = math.floor(axMag * bz / azMag)
    end
    -- sign must be adjusted to be on the correct quadrant
    posx = (bx + axMag) * (axMag / ax)
    posz = (bz + azMag) * (azMag / az)
    return posx, posz
end


-- main search function
function kl_spawn:random_spawn(player, cycle, intent)

    local cycle_max_intent = (math.pow(cycle * 2, 2) - math.pow((cycle - 1) * 2, 2)) * kl_spawn.spawn_count_per_block
    if intent >= cycle_max_intent then
        cycle = cycle + 1
        intent = 0
    end

    --can not found a valid spawn pos
    if cycle > max_cycles then
        minetest.log("error", string.format("[kl_spawn] Can not found a valid spawn position..."))
        --minetest.log('action', string.format("[kl_spawn] Can not found a valid spawn position, set player pos to (0, 0, 0)"))
        --player:set_pos({x = 0, y = 0, z = 0})
    else
        local posx, posz = kl_spawn:get_random_pos(cycle)
        minetest.log(debug_log_level, string.format("[kl_spawn] New Random Spawn Position (%d, %d)", posx, posz))
        if has_valid_height(posx, posz) then
            kl_spawn:find_spawn_elevation(player, {x = posx, y = kl_spawn.min_spawn_elevation, z = posz}, cycle, intent + 1)
        else
            kl_spawn:random_spawn(player, cycle, intent + 1)
        end
    end
end


-- valid pos verification function
function kl_spawn:find_spawn_elevation(player, pos, cycle, intent)

    local posx1, posy1, posz1 =  pos.x - kl_spawn.emerge_radius, kl_spawn.min_spawn_elevation, pos.z - kl_spawn.emerge_radius
    local posx2, posy2, posz2 =  pos.x + kl_spawn.emerge_radius, kl_spawn.max_spawn_elevation, pos.z + kl_spawn.emerge_radius

    minetest.log(debug_log_level, string.format("[kl_spawn] Searching Spawn Position: intent: %d area: (%d, %d, %d) and (%d, %d, %d)",
        intent, posx1, posy1, posz1, posx2, posy2, posz2))

    -- area not fully loaded, wait and force emerge
    if #minetest.find_nodes_in_area({x = posx1, y = posy1, z = posz1}, {x = posx2, y = posy2, z = posz2}, "ignore") > 0 then
        minetest.log(debug_log_level, string.format("[kl_spawn] Area from (%d, %d, %d) to (%d, %d, %d) not fully loaded waiting",
            posx1, posy1, posz1, posx2, posy2, posz2))
        minetest.emerge_area({x = posx1, y = posy1, z = posz1}, {x = posx2, y = posy2, z = posz2})
        minetest.after(kl_spawn.emerge_area_delay,
            (function() kl_spawn:find_spawn_elevation(player, pos, cycle, intent)
            end))

    else
        local node_list = minetest.find_nodes_in_area_under_air({x = posx1, y = posy1, z = posz1}, {x = posx2, y = posy2, z = posz2}, kl_spawn.valid_spawn_node_list)
        minetest.log(debug_log_level, string.format("[kl_spawn] Found %d Posible Spawn Positions between (%d, %d, %d) and (%d, %d, %d)", 
            #node_list, posx1, posy1, posz1, posx2, posy2, posz2))

        --node_list is empty, retry in another position
        if #node_list == 0 then
            kl_spawn:random_spawn(player, cycle, intent)

        --see if node list has a valid position
        else
            local is_valid_pos = false
            local i = 1
            local posAux
            repeat
                posAux = node_list[i]
                minetest.log(debug_log_level, string.format("[kl_spawn] Validating pos (%d, %d, %d)",
                    posAux.x, posAux.y, posAux.z))
                local under_node = minetest.get_node({x = posAux.x, y = posAux.y, z = posAux.z})
                local current_node = minetest.get_node({x = posAux.x, y = posAux.y + 1, z = posAux.z})
                local above_node = minetest.get_node({x = posAux.x, y = posAux.y + 2, z = posAux.z})
                -- check above and under for valid position
                if current_node.name == 'air' and above_node.name == 'air' and under_node.name ~= 'air'
                    and minetest.registered_nodes[under_node.name].drawtype ~= 'liquid'
                    and minetest.registered_nodes[under_node.name].drawtype ~= 'flowingliquid'
                    then

                    is_valid_pos = true
                else
                    i = i + 1
                end
            until i > #node_list or is_valid_pos

            -- valid pos found, set new player pos
            if is_valid_pos then
                minetest.log(debug_log_level, string.format("[kl_spawn] Valid Spawn Position found at position (%d, %d, %d) on node %s",
                    posAux.x, posAux.y, posAux.z, minetest.get_node(posAux).name))
                player:set_pos({x = posAux.x, y = posAux.y + 1, z = posAux.z})
            else
                kl_spawn:random_spawn(player, cycle, intent)
            end
        end
    end
end


-- spawn a new player (can wait if player not exists, for example if world is not generated yet)
function kl_spawn:spawn_new_player(player, intent)
    if player ~= nil then
        kl_spawn:random_spawn(player, 1, 0)
    else
        if intent > 0  then
            minetest.after(kl_spawn.new_player_wait_time, (function() kl_spawn:spawn_new_player(player, intent - 1) end))
        end
    end
end


-- random spam in new player join
minetest.register_on_newplayer(function(player)

    -- initialize lua random seed
    math.randomseed(os.clock())
    kl_spawn:spawn_new_player(player, kl_spawn.new_player_max_intents)

end)


-- On player dead try respawn in random position around dead position
-- if there is not valid position around dead position, it do a full
-- random search calling kl_spawn:random_spawn
minetest.register_on_respawnplayer(function(player)

    if player ~= nil then

        -- check for sethome
		if kl_spawn.sethome_installed then
            local player_name = player:get_player_name()
            local home_pos = kl_sethome.get(player_name)
            if home_pos then
                kl_sethome.go(player_name)
                return true
            end
        end

        -- first try random spawn around dead position
        local player_pos = player:getpos()
        kl_spawn:find_spawn_elevation(player, player_pos, 1, 1)
        return true

    end

end)


-- DEV TOOLS
if kl_spawn.dev_mode then
    minetest.log(debug_log_level, "[KL Spawn] DEV Mode Active, Registering Spawn Test Chat Command")

    minetest.register_chatcommand("search_spawn_pos", {
        params = "[here] | [<x, y, z>]",
		description = "Search a Valid Spawn Position at the specified position, use here to search at player position or leave empty to do a random search",
		func = function(name, param)
            local player = minetest.get_player_by_name(name)
            local cycle_max_intents = (math.pow(max_cycles * 2, 2) - math.pow((max_cycles - 1) * 2, 2)) * kl_spawn.spawn_count_per_block

            if player then
                local player_pos = player:get_pos()
                -- search if there is a valid position around player pos
                if param == "here" then
                    minetest.log(debug_log_level, string.format("[KL Spawn] Player %s at (%d, %d, %d) call Spawn Test Chat Command (here)",
                        name, player_pos.x, player_pos.y, player_pos.z))
                    kl_spawn:find_spawn_elevation(player, player_pos, max_cycles, cycle_max_intents)
                    return true
                end

                local pos = {}
                pos.x, pos.y, pos.z = string.match(param, "^([%d.-]+)[, ] *([%d.-]+)[, ] *([%d.-]+)$")
                pos.x = tonumber(pos.x)
                pos.y = tonumber(pos.y)
                pos.z = tonumber(pos.z)

                -- no parameters full random search
                if not pos.x or not pos.y or not pos.z then
                    minetest.log(debug_log_level, string.format("[KL Spawn] Player %s at (%d, %d, %d) call Spawn Test Chat Command (Random)",
                        name, player_pos.x, player_pos.y, player_pos.z))
                    kl_spawn:random_spawn(player, 1, 0)
                    return true
                end

                if (pos.x < -kl_spawn.radius_max) or (pos.x > kl_spawn.radius_max) or 
                   (pos.y < -kl_spawn.radius_max) or (pos.y > kl_spawn.radius_max) or
                   (pos.z < -kl_spawn.radius_max) or (pos.z > kl_spawn.radius_max) then

                   return false, "[KL Spawn] Position out of range in search_spawn_pos chat command"
                end

                -- search a random valid position around pos indicated by the user
                minetest.log(debug_log_level, string.format("[KL Spawn] Player %s at (%d, %d, %d) call Spawn Test Chat Command with position (%d, %d, %d)",
                    name, player_pos.x, player_pos.y, player_pos.z, pos.x, pos.y, pos.z))
                kl_spawn:find_spawn_elevation(player, player_pos, max_cycles, cycle_max_intents)
                return true

            end
        end,
    })

    minetest.register_chatcommand("random_pos_test", {
        params = "",
		description = "Test the random pos logic",
		func = function(name, param)
            minetest.log(debug_log_level, string.format("[KL Spawn] Random Point Logic Test Start"))
            for cycle = 1, max_cycles, 1 do
                local cycle_max_intents = (math.pow(cycle * 2, 2) - math.pow((cycle - 1) * 2, 2)) * kl_spawn.spawn_count_per_block
                minetest.log(debug_log_level, string.format("[KL Spawn] Random Point Logic Test Cycle %d, Max Intents %d", cycle, cycle_max_intents))
                for intent = 1, cycle_max_intents, 1 do
                    local posx, posz = kl_spawn:get_random_pos(cycle)
                    minetest.log(debug_log_level, string.format("[KL Spawn] Random Point for Cycle %d, Intent %d: (%d, %d)", cycle, intent, posx, posz))
                end
            end
            minetest.log(debug_log_level, string.format("[KL Spawn] Random Point Logic Test End"))
            return true

        end,
    })

end
