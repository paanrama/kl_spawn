CHANGELOG
=========

## 'Unreleased' (2023-10-14)
- Optimized using mapgen_rivers data
- Module mapgen_rivers added to dependencies

## 'v1.0 Beta' (2023-10-13)
- Very slow but working generic logic
